@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="card">
                    <div class="card-header text-center">
                        <h1>Історія продажу</h1>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('saleHistory') }}" method="get" class="mb-3">
                            <div class="row">
                                <div class="col-md-6">
                                    <label for="datepicker">Виберіть дату:</label>
                                    <input type="date" id="datepicker" name="selectedDate" class="form-control">
                                </div>
                                <div class="col-md-6">
                                    <button type="submit" class="btn btn-primary mt-4">Фільтрувати</button>
                                </div>
                            </div>
                        </form>

                        <table class="table table-striped">
                            <thead>
                            <tr>
                                <th>№</th>
                                <th>Назва</th>
                                <th>Кількість</th>
                                <th>Ціна</th>
                                <th>Сума</th>
                                <th>Дата продажу</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($histories as $index => $history)
                                <tr>
                                    <td>{{ $index + 1 }}</td>
                                    <td>{{ $history->name }}</td>
                                    <td>{{ $history->quantity }}</td>
                                    <td>{{ $history->price }}</td>
                                    <td>{{ $history->quantity * $history->price }}</td>
                                    <td>{{ $history->date }}</td>
                                </tr>
                            @endforeach
                            <tr>
                                <td colspan="4"></td>
                                <td><strong>Загальна сума: {{ $totalAmount }}</strong></td>
                                <td></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
