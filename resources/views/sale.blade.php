@extends('layouts.app')

@section('content')
    @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
    @endif
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center"><h1>Товари</h1></div>
                    <div class="card-body">
                        <form action="{{ route('subtractQuantity') }}" method="post">
                            @csrf
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Назва</th>
                                    <th>Залишок</th>
                                    <th>Ціна</th>
                                    <th>Продаж</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sales as $index => $sale)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $sale->name }}</td>
                                        <td>{{ $sale->quantity_total }}</td>
                                        <td>{{ $sale->price }}</td>
                                        <td>
                                            <div class="input-group">
                                                <input type="number" name="quantityToSubtract[{{ $sale->id }}]"
                                                       step="0.01" class="form-control">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">кг.</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-primary">Продаж</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
