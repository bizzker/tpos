@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header bg-primary text-center">
                        <h1>Додати товар</h1>
                    </div>
                    <div class="card-body">
                        <form action="{{ route('addProduct') }}" method="post">
                            @csrf
                            <div class="mb-3">
                                <label for="productName" class="form-label">Назва товару</label>
                                <input type="text" class="form-control" id="productName" name="productName" required>
                            </div>
                            <div class="mb-3">
                                <label for="productTerm" class="form-label">Термін</label>
                                <input type="date" class="form-control" id="productTerm" name="productTerm" required>
                            </div>
                            <div class="text-center">
                                <button type="submit" class="btn btn-success">Додати товар</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
