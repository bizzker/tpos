@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header text-center"><h1>Поставити на прихід</h1></div>
                    <div class="card-body">
                        <form action="{{ route('addQuantity') }}" method="post">
                            @csrf
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>№</th>
                                    <th>Назва</th>
                                    <th>Залишок</th>
                                    <th>Термін</th>
                                    <th>Ціна</th>
                                    <th>Додати кількість</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($sales as $index => $sale)
                                    <tr>
                                        <td>{{ $index + 1 }}</td>
                                        <td>{{ $sale->name }}</td>
                                        <td>{{ $sale->quantity_total }}</td>
                                        <td>{{ $sale->term }}</td>
                                        <td>
                                            <div class="input-group col-md-3">
                                                <input type="number" name="priceToAdd[{{ $sale->id }}]" step="0.01" class="form-control">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">грн.</span>
                                                </div>
                                            </div>
                                        </td>
                                        <td>
                                            <div class="input-group col-md-3">
                                                <input type="number" name="quantityToAdd[{{ $sale->id }}]" step="0.01"
                                                       class="form-control">
                                                <div class="input-group-append">
                                                    <span class="input-group-text">кг.</span>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-primary">Додати</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
