<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Domains\Product\Controllers\ProductController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/sales', [ProductController::class, 'index'])->name('sales.index');
Route::get('/products', [ProductController::class, 'products'])->name('products');
Route::get('/coming', [ProductController::class, 'coming'])->name('coming');
Route::get('/history', [ProductController::class, 'saleHistory'])->name('saleHistory');
Route::post('/addQuantity', [ProductController::class, 'addQuantity'])->name('addQuantity');
Route::post('/subtractQuantity', [ProductController::class, 'subtractQuantity'])->name('subtractQuantity');
Route::post('/addProduct', [ProductController::class, 'addProduct'])->name('addProduct');

