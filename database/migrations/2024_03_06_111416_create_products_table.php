<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    public function up(): void
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('slug')->unique()->default('');
            $table->decimal('quantity_total', 8, 3)->default(0);
            $table->decimal('price', 8, 3)->default(0);
            $table->date('term');
            $table->timestamps();
        });
    }
};
