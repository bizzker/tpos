<?php

namespace App\Domains\Product\Controllers;

use App\Domains\Product\Models\Product;
use App\Domains\Product\Models\SaleHistory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    public function index()
    {
        $sales = Product::orderBy('name')->get();

        return view('sale', compact('sales'));
    }

    public function products()
    {
        return view('add_product');
    }

    public function coming()
    {
        $sales = Product::orderBy('name')->get();

        return view('coming', compact('sales'));
    }

    public function saleHistory(Request $request)
    {
        $query = SaleHistory::orderByDesc('date');

        if ($request->has('selectedDate')) {
            $selectedDate = $request->input('selectedDate');
            $query->whereDate('date', $selectedDate);
        } else {
            $query->whereDate('date', now()->toDateString());
        }

        $histories = $query->get();
        $totalAmount = $histories->sum(function ($history) {
            return $history->quantity * $history->price;
        });

        return view('sale_history', compact('histories', 'totalAmount'));
    }

    public function addQuantity(Request $request)
    {
        $quantityToAdd = $request->input('quantityToAdd');
        $priceToAdd = $request->input('priceToAdd');

        foreach ($quantityToAdd as $saleId => $quantity) {
            $sale = Product::find($saleId);
            $sale->quantity_total += $quantity;

            if (isset($priceToAdd[$saleId])) {
                $sale->price = $priceToAdd[$saleId];
            }

            $sale->save();
        }

        return redirect()->route('coming');
    }

    public function subtractQuantity(Request $request)
    {
        foreach ($request->input('quantityToSubtract') as $saleId => $quantity) {
            if (!is_null($quantity)) {
                $sale = Product::findOrFail($saleId);

                $sale->update(['quantity_total' => $sale->quantity_total - $quantity]);

                SaleHistory::create([
                    'name' => $sale->name,
                    'quantity' => $quantity,
                    'price' => $sale->price,
                    'date' => now(),
                ]);
            }
        }

        return redirect()->route('sales.index')->with('success', 'Продаж успішно здійснено.');
    }


    public function addProduct(Request $request)
    {
        $productName = $request->input('productName');
        $productQuantity = $request->input('productQuantity') ?? 0;
        $productTerm = $request->input('productTerm');

        $slug = Str::slug($productName);

        Product::create([
            'name' => $productName,
            'slug' => $slug,
            'quantity_total' => $productQuantity,
            'term' => $productTerm,
        ]);

        return redirect()->route('sales.index');
    }
}
