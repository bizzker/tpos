<?php

namespace App\Domains\Product\Models;

use App\Domains\Product\Models\Product;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SaleHistory extends Model
{
    use HasFactory;

    protected $table = 'sales_history';
    protected $fillable = ['name', 'quantity', 'price', 'date'];

    public function sale()
    {
        return $this->belongsTo(Product::class);
    }
}
